# webdriverio-docker-seleniumgrid

### Purpose of project
A very simple framework using WebdriverIO along with a Zelenium docker container for Selenium Grid.

### Installing and Running
The project is using `yarn` but you can also use `npm` if required.
Fork the project and run `yarn install`

To execute the tests run `yarn test` this should run the tests using your local machine.

You can run them on a selenium grid by just installing and running Zelenium: https://github.com/zalando/zalenium

The capabilities set in wdio.conf.js is to run all tests on Chome and Firefox.

### Test Reports
The framework will use **Allure** to report on the test results. The reports are localed in **/allure-results** directory.

You will need Allure installed then you can view the reports running the following command: `allure serve`.

If you do not wish to use Allure you can still see the **dot** test results in command line. 