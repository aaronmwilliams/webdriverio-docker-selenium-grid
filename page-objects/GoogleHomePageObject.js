var Page = require('../page-objects/Page');
const logo = 'body #hplogo';
var GoogleHomePageObject = Object.create(Page, {

    open: { value: function() {
        Page.open('');
        browser.waitForVisible(logo);
    }},

    pageTitle: { get: function() { return browser.getTitle(); }},
    checkLogoIsDisplayed: { get: function() { return browser.isVisible(logo); }},

    performSearch: { get: function() {
        const searchBox = 'body  #lst-ib';
        browser.waitForVisible(searchBox);
        browser.setValue(searchBox, 'HelloWorld')
        browser.submitForm('.jsb > center:nth-child(1) > input:nth-child(1)');
        return this.returnFirstResult;
        }},

    returnFirstResult: { get: function() {
        const results = '.r';
        browser.waitForVisible(results);
        return $$(results)[0].getText();
    }}

});

module.exports = GoogleHomePageObject;