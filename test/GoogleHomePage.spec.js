var GoogleHomePageObject = require('../page-objects/GoogleHomePageObject');

var expect = require('chai').expect;

describe('Google UK Homepage', function() {

    before('Navigate to Google', () => {
        GoogleHomePageObject.open();
    })

    it('Should display a logo', function () {
        expect(GoogleHomePageObject.checkLogoIsDisplayed).to.be.true;
    });

    it('Should use correct page title', function () {
        expect(GoogleHomePageObject.pageTitle).to.be.equal('Google');
    });

     it('Should perform search', function() {
         expect(GoogleHomePageObject.performSearch).to.contain("HelloWorld");
         browser.saveScreenshot('GoogleSearch.png');
     });

});